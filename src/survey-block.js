import BaseWidget from './base-widget'
import SurveyCSS from './css/survey.css'


export default class SurveyBlock extends BaseWidget {
    constructor(params, doc) {
        super(params, doc)

        this.container = null
        this.rate = null

        this._build()
    }

    show () {
        this.widget.style.display = 'block'
    }

    hide () {
        this.widget.style.display = 'none'
    }

    _build () {
        const style = this._createElement("style");
        style.textContent = SurveyCSS;
        this.document.getElementsByTagName("head")[0].appendChild(style);

        this.container = this._createElement('div', {
            className: 'main-cont',
            style: {maxWidth: this.params.area_width + 'px'}
        });
        const closeButton = this._createElement('div', {innerHTML: '✕', className: 'close-btn'})
        closeButton.addEventListener('click', this.hide.bind(this))
        const closeContainer = this._createElement('div', {className: 'close-cont'})
        closeContainer.appendChild(closeButton)

        const heightContainer = this._createElement('div', {className: 'height-cont'})
        heightContainer.appendChild(this.container)
        heightContainer.appendChild(closeContainer)

        const wrapper = this._createElement('div', {
            className: 'wrapper',
            style: {
                width: this.params.width + 'px',
                backgroundColor: this.params.background
            }
        });
        wrapper.appendChild(heightContainer)

        this.widget = this._createElement('div', {id: 'omnisurvey'})
        this.widget.appendChild(wrapper)
        this.document.body.appendChild(this.widget)

        this._buildRateLayout()
    }

    _buildRateLayout () {
        const that = this
        if (this.params.width === 0) this.params.width = window.innerWidth;

        const ratesContainer = this._createElement('div', {className: 'rates-cont'})
        for (let i = 1; i <= (this.params.max_rate || 10); i++) {
            const rateBox = this._createElement('div', {className: 'rate-box'})
            let rateEl = this._createElement('div', {
                id: 'rate' + i,
                className: 'rate-el',
                innerHTML: i,
                style: {
                    color: this.params.rate_color,
                    backgroundColor: this.params.rate_background,
                    opacity: this.params.opacity
                }
            });
            rateBox.appendChild(rateEl)
            rateEl.addEventListener('click', function () {
                that.rate = rateEl.innerHTML
                that._buildFeedbackLayout()
            });
            rateEl.addEventListener('mouseenter', function (e) {
                let rate = parseInt(e.target.innerHTML)
                for (let k = 1; k <= rate; k++) {
                    let rateContainer = that.document.querySelector('#rate' + k)
                    rateContainer.style.color = that.params.rate_hover_color
                    rateContainer.style.opacity = 1
                }
            });
            ratesContainer.addEventListener('mouseout', function () {
                let rates = that.document.querySelectorAll('div.rate-el')
                for (let index = 0; index < rates.length; index++) {
                    rates[index].style.color = that.params.rate_color
                    rates[index].style.opacity = that.params.opacity
                }
            });
            ratesContainer.appendChild(rateBox)
        }

        const topTextContainer = this._createElement('div', {className: 'text-cont'})
        topTextContainer.appendChild(this._createElement('div', {
            innerHTML: this.params.rate_title,
            style: {
                color: this.params.color
            }
        }))

        this.container.appendChild(topTextContainer)
        this.container.appendChild(ratesContainer)

        const bottomContainer = this._createElement('div', {className: 'bottom-cont', style: {color: this.params.color}})
        bottomContainer.appendChild(this._createElement('div', {innerHTML: this.params.low_rate}))
        bottomContainer.appendChild(this._createElement('div', {innerHTML: this.params.high_rate}))
        this.container.appendChild(bottomContainer)
    }

    _buildFeedbackLayout () {
        this._clearLayout()

        const topTextContainer = this._createElement('div', {className: 'text-cont'})
        topTextContainer.appendChild(this._createElement('div', {
            innerHTML: this.params.feedback_title, style: {color: this.params.color}
        }));
        this.container.appendChild(topTextContainer)

        const textArea = this._createElement('textarea');
        textArea.setAttribute('placeholder', this.params.feedback_placeholder)
        this.container.appendChild(textArea);

        const feedbackButton = this._createElement('button', {
            innerHTML: this.params.send_feedback,
            style: {
                color: this.params.rate_color,
                backgroundColor: this.params.rate_background,
            }
        })
        const that = this
        feedbackButton.addEventListener('click', function () {
            that.feedback = textArea.value
            that._buildThanyouLayout()
        })

        const buttonContainer = this._createElement('div', {className: 'feedback-btn-cont'})
        buttonContainer.appendChild(feedbackButton)
        this.container.appendChild(buttonContainer)

        if (this.params.copyright) {
            const copyRightContainer = this._createElement('div', {
                style: {backgroundColor: this.params.rate_background}
            })
            const copyRight = this._createElement('div', {
                className: 'copyright',
                style: {
                    backgroundColor: this.params.rate_background,
                    color: this.params.rate_color,
                    maxWidth: this.params.area_width + 'px'
                }
            })
            copyRight.innerHTML = this.params.copyright
            copyRightContainer.appendChild(copyRight)
            this.document.querySelector('#omnisurvey div').appendChild(copyRightContainer)
        }
    }

    _buildThanyouLayout () {
        const data = {widget: this.params.widgetID, rate: this.rate, comment: this.feedback}
        const url = '/' + 'widgets/' + this.siteID + '/'
        fetch(url, {
            method: 'POST',
            mode: 'cors',
            body: JSON.stringify(data)
        });

        this._clearLayout()

        const topTextContainer = this._createElement('div', {className: 'text-cont'});
        topTextContainer.appendChild(this._createElement('div', {
            innerHTML: this.params.thank_you, style: {color: this.params.color}
        }))
        this.container.appendChild(topTextContainer)

        this.widget.style.transition = 'display 3s ease-out 2s'
        window.setTimeout(this.hide.bind(this), 2050)
    }

    _clearLayout () {
        this._deleteChildren(this.container)
    }
}
