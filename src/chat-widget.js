import chatCSS from './css/chat.css'
import chatHTML from './templates/chat.html'
import BaseWidget from './base-widget';


export default class OmniChat extends BaseWidget {
    constructor(params, doc) {
        super(params, doc)
        
        this.ws = null
        this.chatWidget = this.document.getElementById('omnichat')

        this.employees = {}
        this.UID = params.UID

        this.ui = {
            chatInfo: null,
            newMessage: null,
            messages: null,
            lastMessageBlock: null,
            btnSendMessage: null
        }

        this._build()
    }

    show () {
        this.chatWidget.style.height = (this.params.height + 50) + 'px'
        this.chatButton.style.display = 'none'
        this._connect()
    }

    hide () {
        this.chatWidget.style.height = 0
        this.chatButton.style.display = 'block'
    }

    _addMessage (msg) {
        this._setInfo("")

        const msgContainer = this._createElement('div', {
            className: 'message ' + (msg.employee_id ? 'right' : 'left')
        })

        const avatarBlock = this._createElement('div', {className: 'user-pic'})
        let userName = 'You', userImg = ''
        if (msg.employee_id) {
            userName = 'Operator'
            if (this.employees[msg.employee_id]) {
                let employee = this.employees[msg.employee_id]
                userName = employee.name
                userImg = employee.avatar
            }
        }

        // add user pic
        if (userImg) {
            avatarBlock.appendChild(this._createElement('img', {className: 'avatar', src: userImg}))
        } else {
            avatarBlock.appendChild(this._createElement('div', {className: 'avatar', innerHTML: userName.substr(0, 2)}))
        }

        // add time
        if (msg.time) {
            avatarBlock.appendChild(this._createElement('div', {className: 'time', innerHTML: this._getTime(msg.time)}))
        }
        msgContainer.appendChild(avatarBlock)

        const contentBlock = this._createElement('div', {className: 'content'})

        // add user name
        if (msg.type !== 'warning') {
            contentBlock.appendChild(this._createElement('div', {className: 'name', innerHTML: userName}))
        }

        let html = ''
        if (msg.type === 'image') {
            const image = JSON.parse(msg.message)
            html = '<a href="' + image.image + '" target="_blank"><img src="' + image.thumbnail + '" alt="" /></a>'
        } else {
            html = msg.text.replace(/(?:\r\n|\r|\n)/g, '<br />')
        }
        const msgBlock = this._createElement('div', {
            className: 'msg',
            innerHTML: html
        })
        if (msg.type) {
            msgBlock.className += ' type-' + msg.type
        }
        contentBlock.appendChild(msgBlock)
        msgContainer.appendChild(contentBlock)

        this.ui.messages.appendChild(msgContainer)

        this._scrollDown()
    }

    _setInfo (msg, cls) {
        if (cls === undefined) {
            cls = ''
        }
        this.ui.chatInfo.className = cls
        this.ui.chatInfo.innerHTML = msg
    }

    _typeMessage (e) {
        if (e.keyCode === 13) {
            if (this._sendMessage('message', this.ui.newMessage.value.trim())) {
                this.ui.newMessage.value = ''
            }
        } else {
        	this._sendMessage('typing', this.ui.newMessage.value.trim())
        }
    }

    _sendMessage (command, text) {
        let msg = {
            command: command,
            text: text,
            employee_id: null,
            time: (new Date()).getTime()
        };
        return this._sendData(msg)
    }

    _scrollDown () {
        this.ui.messages.scrollTop = this.ui.messages.scrollHeight
    }

    _build () {
        this._buildChat()
        this._buildChatButton()
    }

    _buildChat () {
        if (this.chatWidget === null) {
            const style = this._createElement("style")
            style.textContent = chatCSS
            this.document.getElementsByTagName("head")[0].appendChild(style)

            this.chatWidget = this._createElement('div', {'id': 'omnichat', 
                'style': {
                    bottom: this.params.bottom + 'px',
                    right: this.params.right + 'px',
                    height: (this.params.height + 45) + 'px',
                    width: this.params.width + 'px'
                }
            })
            this.chatWidget.innerHTML = chatHTML
            this.document.body.appendChild(this.chatWidget)

            // fix chat header styles
            const header = this.document.getElementById('omnichat__header')
            header.style.color = this.params.primary_font_color
            header.style.backgroundColor = this.params.primary_color

            this.document.getElementById('omnichat_header_text').innerHTML = this.params.chat_header_online
            this.document.querySelector('#omnichat__chat').style.height = this.params.height + 'px'
        }

        this._bindUI()
    }

    _bindUI () {
        this.document.querySelector('#omnichat__header .close-cont').addEventListener('click', this.hide.bind(this))

        this.ui.chatInfo = this.document.getElementById('omnichat__info')
        this.ui.newMessage = this.document.getElementById('omnichat__message')
        this.ui.messages = this.document.getElementById('omnichat__messages')
        this.ui.btnSendMessage = this.document.getElementById('omnichat__send_message')

        // bind handlers
        this.ui.newMessage.addEventListener('keyup', this._typeMessage.bind(this))
        if (this.ui.btnSendMessage) {
            this.ui.btnSendMessage.addEventListener('click', this._typeMessage.bind(this))
        }

        const that = this
        this.document.getElementById('upload-icon').addEventListener('click', function () {
            that.document.getElementById('upload-form').contentWindow.document.getElementById('file').click()
        })
    }

    _buildChatButton () {
        this.chatButton = this._createElement('div', {
            id: 'omnichat_label',
            style: {
                bottom: this.params.bottom + 'px',
                right: this.params.right + 'px',
                width: this.params.width + 'px',
                color: this.params.primary_font_color,
                backgroundColor: this.params.primary_color
            }
        })

        this.chatButton.addEventListener('click', this.show.bind(this))
        this.chatButton.appendChild(
            this._createElement('div', {className: 'text', innerHTML: this.params.call_header_online}))
        this.document.body.appendChild(this.chatButton);
    }

    _connect () {
        if (this.ws && this.ws.readyState === 1) return

        this.ws = new WebSocket('ws://' + this.params.host + '/ws/chat?r=' + (new Date().getTime()))

        const that = this
        this.ws.onopen = function() {
        	setTimeout(function() {
        		that._sendData({command: 'init', uid: that.UID, channel: that.params.channel})
        	}, 200);
        }
        this.ws.onmessage = this._receiveData.bind(this);
        this.ws.onerror = function (err) {
            console.log(err)
        }
        this.ws.onclose = function() {
            setTimeout(function () {
                that._connect();
            }, 3000)
        }
    }

    _receiveData (event) {
        let data = JSON.parse(event.data)
        console.log(data)

        switch (data.command) {
            case 'error':
                this.ui.newMessage.disabled = true
                this._addMessage({
                    message: data.message, type: 'warning', time: (new Date()).getTime()
                })
                break;

            case 'employees':
                this.employees = {}
                for (let idx in data.employees) {
                    this.employees[data.employees[idx].id] = data.employees[idx]
                }
                break;

            case 'image':
            case 'message':
                this._addMessage(data)
                break;

            case 'typing':
                let msg = data.employee_id ? 'Оператор' : 'Собеседник'
                msg += ' печатает....'
            	this._setInfo(msg)
            	break;

            case 'form':
                if (this.employee) break
                let fields = JSON.parse(data.message)
                console.log(fields)
                break;
        }
    }

    _sendData (data) {
        if (this.ws.readyState !== 1) {
        	console.log('Websocket is not ready')
        	return false
        }
        console.log(data)
        this.ws.send(JSON.stringify(data))
        return true
    }
}
