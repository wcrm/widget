export default class BaseWidget {
    constructor(params, doc) {
        this.params = params || {}
        this.document = doc || document
    }
    
    _createElement (tagName, options) {
        let el = this.document.createElement(tagName)
        options = options || {}

        if (options.id) {
            el.setAttribute('id', options.id)
        }
        if (options.className) {
            el.className = options.className
        }
        if (options.innerHTML) {
            el.innerHTML = options.innerHTML
        }
        if (options.src) {
            el.src = options.src
        }

        if (options.style) {
            for (var a in options.style) {
                el.style[a] = options.style[a]
            }
        }
        return el
    }

    _deleteChildren(node) {
        while (node.firstChild) {
            node.removeChild(node.firstChild)
        }
    }

    _getTime (time) {
        let dt;
        if (typeof(time) === 'number') {
            dt = new Date(time * 1000)
        } else if (typeof(time) === 'string') {
            dt = new Date(parseInt(time) * 1000)
        } else {
            return ''
        }
        return ('0' + dt.getHours()).slice(-2) + ':' + ('0' + dt.getMinutes()).slice(-2)
    }
}