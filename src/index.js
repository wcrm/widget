import OmniChat from './chat-widget'
import NewsWidget from './news-widget'
import SurveyWidget from './survey-block'


class OmniWidget {
    constructor(currentWindow, currentDocument) {
        this.window = currentWindow || window
        this.document = currentDocument || document

        this.host = 'localhost:8000'
        this.siteID = window.OmniSiteID || null
        this.params = window.OmniParams || {}

        this.UID = this._getCookie('omniuid')
        if (this.UID === undefined || this.UID === 'undefined') {
            this.UID = this._generateUUID4()
            this._setCookie('omniuid', this.UID)
        }
        this.sessionID = this._getCookie('omnisession')

        this._loadData()
    }

    getClientInfo() {
        return {
            time_opened: new Date(),
            timezone: (new Date()).getTimezoneOffset() / 60,

            page: window.location.pathname,
            referrer: document.referrer,
            previousSites: history.length,

            browser: {
                name: navigator.appName,
                engine: navigator.product,
                language: navigator.language,
                version1a: navigator.appVersion,
                version1b: navigator.userAgent,
                online: navigator.onLine,
                platform: navigator.platform
            },
            sizes: {
                screenWidth: screen.width,
                screenHeight: screen.height,
                documentWidth: document.width,
                documentHeight: document.height,
                innerWidth: innerWidth,
                innerHeight: innerHeight,
                availableWidth: screen.availWidth,
                availableHeight: screen.availHeight,
                colorDepth: screen.colorDepth,
                pixelDepth: screen.pixelDepth
            }
        }
    }

    _loadData() {
        this.serverWidgetsURL = 'http://' + this.host + '/public/widgets/'
        this.serverHeaders = {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        };

        let initialData = {
            site: this.siteID,
            uid: this.UID,
            session: this.sessionID,
            title: document.title,
            url: document.URL,
            data: this.getClientInfo()
        };

        const that = this
        fetch(this.serverWidgetsURL, {
            method: 'POST',
            mode: 'cors',
            headers: that.serverHeaders,
            body: JSON.stringify(initialData)
        }).then(
            function (response) {
                if (response.status !== 200) {
                    console.log('Looks like there was a problem. Status Code: ' + response.status);
                    return;
                }

                response.json().then(function (data) {
                    that._assignData(data);
                });
            }
        ).catch(function (err) {
            console.log('Get initial data error:', err);
            that._assignData({
                uid: 'ff3c96fa-6dd4-489d-95e9-132eb314ecb9',
                session: 'ff3c96fa-6dd4-489d-95e9-132eb314ecb8',
                widgets: [{
                    type: 'chat',
                    params: {
                        host: '127.0.0.1:3000',
                        bottom: 20,
                        right: 40,
                        height: 450,
                        width: 300,
                        primary_color: '#1e88e5',
                        primary_font_color: '#fff',
                        call_header_online: 'We are online',
                        chat_header_online: 'Chat us. We are here',
                        channel: 'f9fa7644-0f4b-451f-a4cb-5373cd3ddee1'
                    }
                }, {
                    type: 'survey',
                    params: {
                        width: 400,
                        max_rate: 10,
                        color: '#000',
                        background: '#fff',

                        rate_color: '#fff',
                        rate_background: '#1E88E5',
                        rate_hover_color: '',
                        opacity: '0.5',

                        rate_title: 'Оцените наш сервис?',
                        low_rate: 'Не нравится',
                        high_rate: 'Очень нравится',
                        feedback_title: 'Что мы можем улучшить??',
                        feedback_placeholder: 'Напишите свои предложения здесь...',
                        send_feedback: 'Отправить отзыв',
                        'copyright': 'Build by OmniCRM'
                    }
                }, {
                    type: 'news',
                    params: {
                        left: 10,
                        title: "Новый раздел - Уцененные товары",
                        description: 'У нас открылся новый раздел с уцененными товарами'
                    }
                }]
            })
        });
    }

    _assignData(data) {
        // if (this.UID !== data.uid) {
        //     this.UID = data.uid;
        //     this._setCookie('omniuid', this.UID, true);
        // }
        // if (this.sessionID !== data.session) {
        //     this.sessionID = data.session;
        //     this._setCookie('omnisession', this.sessionID);
        // }

        // run widgets
        for (let w in data.widgets) {
            let widget = data.widgets[w];
            widget.params.UID = this.UID;
            widget.params.widgetID = widget.id;

            switch (widget.type) {
                case 'chat':
                    this.Chat = new OmniChat(widget.params, this.document);
                    if (1) this.Chat.hide()
                    break;

                case 'news':
                    this.newsWidget = new NewsWidget(widget.params, this.document);
                    if (1) this.newsWidget.show(widget.params, this.document)
                    break;

                case "survey":
                    this.surveyWidget = new SurveyWidget(widget.params, this.document);
                    break;
            }
        }
    }

    _getCookie(name) {
        const matches = this.document.cookie.match(new RegExp(
            "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
        ))
        return matches ? decodeURIComponent(matches[1]) : undefined
    }

    _setCookie(name, value, options) {
        options = options || {};
        let expires = options.expires;

        if (typeof expires === "number" && expires) {
            let d = new Date();
            d.setTime(d.getTime() + expires * 1000);
            expires = options.expires = d;
        }
        if (expires && expires.toUTCString) {
            options.expires = expires.toUTCString();
        }

        let updatedCookie = name + "=" + encodeURIComponent(value);
        for (let propName in options) {
            updatedCookie += "; " + propName;
            let propValue = options[propName];
            if (propValue !== true) {
                updatedCookie += "=" + propValue;
            }
        }

        this.document.cookie = updatedCookie;
    }

    _generateUUID4 () {
        //// return uuid of form xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx
        let uuid = '', ii;
        for (ii = 0; ii < 32; ii += 1) {
          switch (ii) {
          case 8:
          case 20:
            uuid += '-';
            uuid += (Math.random() * 16 | 0).toString(16);
            break;
          case 12:
            uuid += '-';
            uuid += '4';
            break;
          case 16:
            uuid += '-';
            uuid += (Math.random() * 4 | 8).toString(16);
            break;
          default:
            uuid += (Math.random() * 16 | 0).toString(16);
          }
        }
        return uuid;
    }
}


new OmniWidget(window);
