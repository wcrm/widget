import BaseWidget from './base-widget'


export default class NewsWidget extends BaseWidget {
    constructor(params, doc) {
        super(params, doc)

        this.newsWidget = null
    }

    show (content) {
        this.content = content || {}
        this._build()
        this.newsWidget.style.display = 'show'
    }

    hide () {
        this.newsWidget.style.display = 'none'
    }

    _build () {
        if (this.newsWidget === null) {
            this._buildUI()
        }
    }

    _buildUI () {
        const style = this._createElement("style");
        style.textContent = '#omninews{position:fixed;min-width:200px;max-width:400px;border:solid 1px #fff;font-family:sans-serif;font-size:16px;z-index:99999;bottom:10px;right:10px;border-radius:3px;box-shadow:0 12px 25px 8px rgba(0,0,0,.17);background:#fff}#omninews .close-cont{position:absolute;top:5px;right:5px}#omninews .close-cont .close-btn{padding:5px;cursor:pointer;color:#51A3FF;font-size:11px;}#omninews .main-cont{padding:10px 15px 15px;}#omninews .title{font-size:13px;font-weight:bold;text-decoration:none}#omninews .descr{margin:5px 0 0;font-size:12px;text-decoration:none;display:block}';
        document.getElementsByTagName("head")[0].appendChild(style);

        const mainContainer = this._createElement('div', {className: 'main-cont'});
        const closeButton = this._createElement('div', {innerHTML: '✕', className: 'close-btn'});
        const closeContainer = this._createElement('div', {className: 'close-cont'});
        closeContainer.addEventListener('click', this.hide.bind(this));
        closeContainer.appendChild(closeButton);
        mainContainer.appendChild(closeContainer);

        const title = this._createElement('a', {
            className: 'title',
            innerHTML: this.content.title,
            style: {
                color: this.params.title_color || '#000'
            }
        });
        if (this.content.url) {
            title.setAttribute('href', this.content.url);
            title.setAttribute('target', '_blank');
        }
        mainContainer.appendChild(title);

        const description = this._createElement('a', {
            className: 'descr',
            innerHTML: this.content.description,
            style: {
                color: this.params.descr_color || '#000',
            }
        });
        if (this.params.url_on_descr && this.content.url) {
            description.setAttribute('href', this.content.url);
            description.setAttribute('target', '_blank');
        }
        mainContainer.appendChild(description);

        let newsStyle = {
            width: this.params.width + 'px',
            borderColor: this.params.border_color,
            backgroundColor: this.params.background
        }
        if (this.params.left) newsStyle.left = this.params.left + 'px'
        if (this.params.right) newsStyle.left = this.params.right + 'px'
        if (this.params.bottom) newsStyle.left = this.params.bottom + 'px'
        this.newsWidget = this._createElement('div', {
            id: 'omninews',
            style: newsStyle,
        });
        this.newsWidget.appendChild(mainContainer);
        document.body.appendChild(this.newsWidget);
    }
}
